﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UITestAPIApplication.Models
{
    public interface IUITestDataContext : IDisposable
    {
        DbSet<Loan> Loans { get; }
        int SaveChanges();
        void MarkAsModified(Loan item);
    }
}
