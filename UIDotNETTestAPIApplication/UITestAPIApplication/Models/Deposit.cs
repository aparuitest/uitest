namespace UITestAPIApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Deposit")]
    public partial class Deposit
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string DepositType { get; set; }

        public double DepositAmount { get; set; }

        public double DepositInterestRate { get; set; }

        public int? TenureYears { get; set; }

        [StringLength(10)]
        public string LastUpdatedBy { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool? ActiveFlag { get; set; }

        public int CustomerId { get; set; }

        [StringLength(150)]
        public string DepositMaturityIns { get; set; }

        public int? TenureMonths { get; set; }

        public int? TenureDays { get; set; }
    }
}
