namespace UITestAPIApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        public string LastName { get; set; }

        [Required]
        [StringLength(100)]
        public string Address { get; set; }

        [Required]
        [StringLength(50)]
        public string City { get; set; }

        public long MobileNumber { get; set; }

        public int PinCode { get; set; }

        [Required]
        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(10)]
        public string CustomerType { get; set; }

        [StringLength(10)]
        public string LastUpdatedBy { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool? ActiveFlag { get; set; }
    }
}
