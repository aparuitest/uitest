namespace UITestAPIApplication.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class UITestDataModel : DbContext,IUITestDataContext
    {
        public UITestDataModel()
            : base("name=UITestDataModel")
        {
        }

        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<Deposit> Deposits { get; set; }
        public virtual DbSet<Loan> Loans { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .Property(e => e.LastUpdatedBy)
                .IsFixedLength();

            modelBuilder.Entity<Deposit>()
                .Property(e => e.LastUpdatedBy)
                .IsFixedLength();

            modelBuilder.Entity<Loan>()
                .Property(e => e.LastUpdatedBy)
                .IsFixedLength();
        }

        public void MarkAsModified(Loan item)
        {
            Entry(item).State = EntityState.Modified;
        }
    }
}
