namespace UITestAPIApplication.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Loan")]
    public partial class Loan
    {
        public int Id { get; set; }

        [Required]
        [StringLength(10)]
        public string LoanType { get; set; }

        public double LoanAmount { get; set; }

        public double LoanInterestRate { get; set; }

        [StringLength(50)]
        public string FixedFloatingFlag { get; set; }

        public int TenureMonths { get; set; }

        [StringLength(10)]
        public string LastUpdatedBy { get; set; }

        public DateTime? LastUpdateTime { get; set; }

        public bool? ActiveFlag { get; set; }

        public int CustomerId { get; set; }

        public double? BalanceAmt { get; set; }

        public double? IntAmt { get; set; }

        public double? RePayment { get; set; }

        public double? PayCarryOverAmt { get; set; }
    }
}
