﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UITestAPIApplication.Models;

namespace UITestAPIApplication.Tests
{
    class TestLoanDbSet : TestDbSet<Loan>
    {
        public override Loan Find(params object[] keyValues)
        {
            return this.FirstOrDefault(loan => loan.Id == (int)keyValues.Single());
        }
    }
}
