﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using UITestAPIApplication;
using UITestAPIApplication.Controllers;
using UITestAPIApplication.Models;

namespace UITestAPIApplication.Tests.Controllers
{
    [TestClass]
    public class LoansControllerTest
    {
        [TestMethod]        
        public void GetLoans_ShouldReturnAllLoans()
        {
            var context = new TestUITestAppContext();
            context.Loans.Add(new Loan { Id = 1, LoanType = "Demo1", LoanAmount = 200, CustomerId=1 });
            context.Loans.Add(new Loan { Id = 2, LoanType = "Demo2", LoanAmount = 300, CustomerId = 1 });
            context.Loans.Add(new Loan { Id = 3, LoanType = "Demo3", LoanAmount = 4000, CustomerId = 2 });
            //For Customer id 1
            var controller = new LoansController(context); 
            IQueryable<Loan> actionResult = controller.GetLoans(1);

            //Assert
            Assert.IsNotNull(actionResult);
            Assert.AreEqual(actionResult.AsEnumerable().Count(),2);

           
        }
        [TestMethod]
        public void PutProduct_ShouldReturnStatusCode()
        {
            var controller = new LoansController(new TestUITestAppContext());

            var item = GetDemoLoan();

            var result = controller.PutLoan(item.Id, item) as StatusCodeResult;
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(StatusCodeResult));
            Assert.AreEqual(HttpStatusCode.NoContent, result.StatusCode);
        }
        Loan GetDemoLoan()
        {
            return new Loan() { Id = 1, LoanType = "Demo1", LoanAmount = 200, CustomerId = 1, ActiveFlag = false};
        }

    }
}
