﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UITestAPIApplication.Models;

namespace UITestAPIApplication.Tests
{
    public class TestUITestAppContext : IUITestDataContext
    {
        public TestUITestAppContext()
        {
            this.Loans = new TestLoanDbSet();
        }

        public DbSet<Loan> Loans { get; set; }

        public int SaveChanges()
        {
            return 0;
        }

        public void MarkAsModified(Loan item) { }
        public void Dispose() { }

    }
}
