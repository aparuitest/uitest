import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators/catchError';
import { _throw } from 'rxjs/observable/throw';

import { Loan } from '../models/loan'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoandashboardService {

  private valuesUrl = environment.apiUrl + 'api/';

  constructor(private http: Http, private httpclient: HttpClient) { }

  getLoansForCustomer(custId: string): Observable<any[]> {

    let apiUrl = this.valuesUrl + "Loans?CustId=" + custId;
    console.log(apiUrl, "Url");
    return this.http.get(apiUrl)
      .map((response: Response) => {
        const data = response.json();
        // console.log(data ,"in service");
        return data;
      });
  }

  postTopUpApply(loanid: any, loanobject: any) {

    let hdrs = new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
    let url = this.valuesUrl + "Loans/" + loanid;

    return this.httpclient.put(url, loanobject, { headers: hdrs, observe: 'response' });
  }

}
