import { TestBed, inject } from '@angular/core/testing';

import { LoandashboardService } from './loandashboard.service';

describe('LoandashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LoandashboardService]
    });
  });

  it('should be created', inject([LoandashboardService], (service: LoandashboardService) => {
    expect(service).toBeTruthy();
  }));
});
