import { Component, OnInit } from '@angular/core';
import { LoandashboardService } from '../../services/loandashboard.service';
//import { Loan } from '../../Models/loan';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Observable } from "rxjs/Observable";
import { MatExpansionModule } from '@angular/material/expansion';
import { NgForm } from "@angular/forms";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [LoandashboardService]
})
export class DashboardComponent implements OnInit {
  arr = [];
  characters: any[];
  sub: any;
  id: number;
  carryamtforui: any;
  loanid: any;
  flag: any;
  loanobject: any;

  constructor(private loandashboardservice: LoandashboardService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.route.queryParams.subscribe(queryparams => {
      this.id = +queryparams['customerid'];
    });
    console.log(this.id, "customerid");


    this.loandashboardservice.getLoansForCustomer(this.id.toString()).subscribe(data => {
      console.log(data, "service data")
      this.characters = data;      
    });
    console.log(this.characters);
  }

  onSelect(selectedItem: any) {
    console.log("Selected item Id: ", selectedItem.PayCarryOverAmt); // You get the Id of the selected item here
    this.carryamtforui = selectedItem.PayCarryOverAmt;
    this.loanid = selectedItem.Id;
    this.loanobject = selectedItem;
    console.log(this.loanid, "Loan Id 1");
    console.log(this.loanobject, "Loan Object 1");

  }
  applyTopUp() {
    let res: Response;
    //let urlData =  "?"+"loanid=" + this.loanid;
    console.log(this.loanid, "Loan Id 2");
    console.log(this.loanobject, "Loan Object 2");
    this.loanobject.ActiveFlag = true; //For loan top up
    this.loandashboardservice.postTopUpApply(this.loanid, this.loanobject).subscribe(res => {
      this.flag = res.toString();
      console.log(res, "response");
      alert("Top Up Submitted Successfully");
     
    });
  }
}