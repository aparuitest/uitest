export interface Loan {
    Id: number,
    userName: string,
    BalanceAmt:number,
    IntAmt: number,
    RePayment:number,
    PayCarryOverAmt:number,
    LoanType:string,
    CustomerId:number,
    LoanAmount:number,
    ActiveFlag:boolean,
    LoanInterestRate:number,
    FixedFloatingFlag:string,
    TenureMonths:number,
    LastUpdatedBy:string,
    LastUpdateTime:Date
}
