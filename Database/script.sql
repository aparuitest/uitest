USE [master]
GO
/****** Object:  Database [UITestBank]    Script Date: 9/11/2018 5:51:50 PM ******/
CREATE DATABASE [UITestBank]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UITestBank', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\UITestBank.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'UITestBank_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\UITestBank_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [UITestBank] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UITestBank].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UITestBank] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UITestBank] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UITestBank] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UITestBank] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UITestBank] SET ARITHABORT OFF 
GO
ALTER DATABASE [UITestBank] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UITestBank] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UITestBank] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UITestBank] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UITestBank] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UITestBank] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UITestBank] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UITestBank] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UITestBank] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UITestBank] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UITestBank] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UITestBank] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UITestBank] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UITestBank] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UITestBank] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UITestBank] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UITestBank] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UITestBank] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UITestBank] SET  MULTI_USER 
GO
ALTER DATABASE [UITestBank] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UITestBank] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UITestBank] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UITestBank] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [UITestBank] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [UITestBank] SET QUERY_STORE = OFF
GO
USE [UITestBank]
GO
ALTER DATABASE SCOPED CONFIGURATION SET IDENTITY_CACHE = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [UITestBank]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 9/11/2018 5:51:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Address] [nvarchar](100) NOT NULL,
	[City] [nvarchar](50) NOT NULL,
	[MobileNumber] [bigint] NOT NULL,
	[PinCode] [int] NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[CustomerType] [nvarchar](10) NULL,
	[LastUpdatedBy] [nchar](10) NULL,
	[LastUpdateTime] [datetime] NULL,
	[ActiveFlag] [bit] NULL,
 CONSTRAINT [PK_dbo.Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Deposit]    Script Date: 9/11/2018 5:51:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Deposit](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[DepositType] [nvarchar](10) NOT NULL,
	[DepositAmount] [float] NOT NULL,
	[DepositInterestRate] [float] NOT NULL,
	[TenureYears] [int] NULL,
	[LastUpdatedBy] [nchar](10) NULL,
	[LastUpdateTime] [datetime] NULL,
	[ActiveFlag] [bit] NULL,
	[CustomerId] [int] NOT NULL,
	[DepositMaturityIns] [nvarchar](150) NULL,
	[TenureMonths] [int] NULL,
	[TenureDays] [int] NULL,
 CONSTRAINT [PK_dbo.Deposit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Loan]    Script Date: 9/11/2018 5:51:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Loan](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoanType] [nvarchar](10) NOT NULL,
	[LoanAmount] [float] NOT NULL,
	[LoanInterestRate] [float] NOT NULL,
	[FixedFloatingFlag] [nvarchar](50) NULL,
	[TenureMonths] [int] NOT NULL,
	[LastUpdatedBy] [nchar](10) NULL,
	[LastUpdateTime] [datetime] NULL,
	[ActiveFlag] [bit] NULL,
	[CustomerId] [int] NOT NULL,
	[BalanceAmt] [float] NULL,
	[IntAmt] [float] NULL,
	[RePayment] [float] NULL,
	[PayCarryOverAmt] [float] NULL,
 CONSTRAINT [PK_dbo.Loan] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (4, N'Aparna', N'Jiddu', N'ITPL', N'Bangalore', 9900565746, 560037, N'India', N'Corporate', NULL, NULL, NULL)
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (5, N'Deepti', N'Nahar', N'Sahyadri Park', N'Pune', 87654433, 456789, N'India', N'Individual', NULL, NULL, NULL)
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (6, N'Test', N'No test', N'ITPL', N'Bangalore', 9865367899, 560018, N'India', N'Corporate', NULL, NULL, NULL)
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (8, N'TCS', N'Company', N'ITPL', N'Bangalore', 2742389473, 560018, N'India', N'Corporate', NULL, NULL, NULL)
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (9, N'aparna', N'Varanasi', N'ITPL', N'fg', 453453454, 4534, N'India', N'Individual', NULL, NULL, NULL)
INSERT [dbo].[Customer] ([Id], [FirstName], [LastName], [Address], [City], [MobileNumber], [PinCode], [Country], [CustomerType], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag]) VALUES (10, N'aparna', N'V', N'ITPL', N'fg', 453453454, 4534, N'India', N'Individual', NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Customer] OFF
SET IDENTITY_INSERT [dbo].[Deposit] ON 

INSERT [dbo].[Deposit] ([Id], [DepositType], [DepositAmount], [DepositInterestRate], [TenureYears], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [DepositMaturityIns], [TenureMonths], [TenureDays]) VALUES (1, N'Term', 10000, 8, 1, NULL, NULL, NULL, 4, N'RenewPandI', 0, 0)
SET IDENTITY_INSERT [dbo].[Deposit] OFF
SET IDENTITY_INSERT [dbo].[Loan] ON 

INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (1, N'Home', 1234, 1, N'Fixed', 12, NULL, NULL, 1, 1, 1334, 100, 1334, 2000)
INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (2, N'Vehicle', 3000, 3, N'Floating', 23, NULL, NULL, NULL, 2, NULL, NULL, NULL, NULL)
INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (3, N'Vehicle', 1600, 1, N'Fixed', 12, NULL, NULL, NULL, 1, 1284, 50, 1284, 2000)
INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (4, N'Home', 1000, 5, N'Fixed', 35, NULL, NULL, NULL, 2, 1100, 100, NULL, NULL)
INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (5, N'Personal', 500, 4, N'Fixed', 56, NULL, NULL, NULL, 2, 525, 25, 500, 1000)
INSERT [dbo].[Loan] ([Id], [LoanType], [LoanAmount], [LoanInterestRate], [FixedFloatingFlag], [TenureMonths], [LastUpdatedBy], [LastUpdateTime], [ActiveFlag], [CustomerId], [BalanceAmt], [IntAmt], [RePayment], [PayCarryOverAmt]) VALUES (6, N'Vehicle', 435, 41, N'Fixed', 3, NULL, NULL, NULL, 5, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Loan] OFF
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('system') FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT (getdate()) FOR [LastUpdateTime]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ('true') FOR [ActiveFlag]
GO
ALTER TABLE [dbo].[Deposit] ADD  DEFAULT ('system') FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[Deposit] ADD  DEFAULT (getdate()) FOR [LastUpdateTime]
GO
ALTER TABLE [dbo].[Deposit] ADD  DEFAULT ('true') FOR [ActiveFlag]
GO
ALTER TABLE [dbo].[Loan] ADD  CONSTRAINT [DF__Loan__LastUpdate__1A14E395]  DEFAULT (suser_sname()) FOR [LastUpdatedBy]
GO
ALTER TABLE [dbo].[Loan] ADD  DEFAULT (getdate()) FOR [LastUpdateTime]
GO
ALTER TABLE [dbo].[Loan] ADD  DEFAULT ('true') FOR [ActiveFlag]
GO
USE [master]
GO
ALTER DATABASE [UITestBank] SET  READ_WRITE 
GO
